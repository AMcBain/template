/** Called automatically by JsDoc Toolkit. */
function publish(symbolSet) {
	publish.conf = {  // trailing slash expected for dirs
		url:         "",
		ext:         ".html",
		outDir:      JSDOC.opt.d || SYS.pwd+"../out/jsdoc/",
		templatesDir: JSDOC.opt.t || SYS.pwd+"../templates/+template/",
		symbolsDir:  "symbols/",
		srcDir:      "symbols/src/",
		templateName: "+Template",
		templateLink: "https://bitbucket.org/AMcBain/template/"
	};
	IO.copyFile(publish.conf.templatesDir+"/static/default.css", publish.conf.outDir, "default.css");

	publish.summtags = {};
	if (JSDOC.opt.D.summtags && JSDOC.opt.D.summtags.indexOf(";") !== -1) {
		var summtag, space, summtags = JSDOC.opt.D.summtags.split(/;\s+?/);

		for (var i = 0, l = summtags.length; i < l; i++) {
			summtag = summtags[i].trim();

			if ((space = summtag.indexOf(" ")) !== -1) {
				publish.summtags[summtag.substring(0, space)] = summtag.substring(space+1).trim();
			} else {
				publish.summtags[summtag] = summtag;
			}
		}
	}
	
	// is source output is suppressed, just display the links to the source file
	if (JSDOC.opt.s && defined(Link) && Link.prototype._makeSrcLink) {
		Link.prototype._makeSrcLink = function(srcFilePath) {
			return "&lt;"+srcFilePath+"&gt;";
		}
	}
	
	// create the folders and subfolders to hold the output
	IO.mkPath((publish.conf.outDir+"symbols").split("/"));
	if (!JSDOC.opt.s) {
	IO.mkPath((publish.conf.outDir+"symbols/src").split("/"));
	}
		
	// used to allow Link to check the details of things being linked to
	Link.symbolSet = symbolSet;

	// create the required templates
	try {
		var classTemplate = new JSDOC.JsPlate(publish.conf.templatesDir+"class.tmpl");
		var classesTemplate = new JSDOC.JsPlate(publish.conf.templatesDir+"allclasses.tmpl");
		var headerTemplate = new JSDOC.JsPlate(publish.conf.templatesDir+"header.tmpl");
	}
	catch(e) {
		print("Couldn't create the required templates: "+e);
		quit();
	}
	
	// some ustility filters
	function hasNoParent($) {return ($.memberOf == "")}
	function isaFile($) {return ($.is("FILE"))}
	function isaClass($) {return ($.is("CONSTRUCTOR") || $.isNamespace)}
	
	// get an array version of the symbolset, useful for filtering
	var symbols = symbolSet.toArray();
	
	// create the hilited source code files
	var files = JSDOC.opt.srcFiles;
	if (!JSDOC.opt.s) {
 	for (var i = 0, l = files.length; i < l; i++) {
 		var file = files[i];
 		var srcDir = publish.conf.outDir + "symbols/src/";
		makeSrcFile(file, srcDir);
 	}
	}
	
 	// get a list of all the classes in the symbolset
 	var classes = symbols.filter(isaClass).sort(makeSortby("alias"));
	
	// create a filemap in which outfiles must be to be named uniquely, ignoring case
	if (JSDOC.opt.u) {
		var filemapCounts = {};
		Link.filemap = {};
		for (var i = 0, l = classes.length; i < l; i++) {
			var lcAlias = classes[i].alias.toLowerCase();
			
			if (!filemapCounts[lcAlias]) filemapCounts[lcAlias] = 1;
			else filemapCounts[lcAlias]++;
			
			Link.filemap[classes[i].alias] = 
				(filemapCounts[lcAlias] > 1)?
				lcAlias+"_"+filemapCounts[lcAlias] : lcAlias;
		}
	}
	
	var spaces = [];
	var spaceMap = {};
	var callMap = {};
	var augMap = {};
	Link.base = "../";
	
	for (var i = 0, l = classes.length; i < l; i++) {
		var symbol = classes[i];
		
		if (JSDOC.opt.D.hierarchy) {
			var levelUp = symbol.alias.substring(0, symbol.alias.lastIndexOf("."));
			
			if (spaceMap[levelUp]) {
				
				var index = spaces.length - 1;
				while (index >= 0 && levelUp !== spaces[index]) {
				
					index--;
				}
				if (index < 0) {
					spaces.push(levelUp);
				} else if (index >= 0) {
					spaces = spaces.slice(0, index + 1);
				}
				
				symbol.parentSpace = levelUp;
				symbol.parentSpaceIndent = spaces.length;
			} else {
				spaces = [];
			}
			spaceMap[symbol.alias] = true;
		}
		
		symbol.events = symbol.getEvents();   // 1 order matters
		symbol.methods = symbol.getMethods(); // 2
		
		if (JSDOC.opt.D.calls) {
			// Take it slow. Ha ha, that's exactly what'll happen if this is used a lot on a large codebase.
			// If I wasn't trying to keep this within the template, the core could do this with less effort.
			for (var j = 0, m = symbol.methods.length, p = symbol.events.length; j < m + p; j++) {
				var method = (j < m ? symbol.methods[j] : symbol.events[j - m]);
				method.calls = method.comment.getTag("calls");
				
				if (!callMap[method.alias]) {
					callMap[method.alias] = [];
				}
				method.calledBy = callMap[method.alias];
				
				// If inerhited methods are handled, the called-by listings can have duplicate entries.
				if (method.calls.length && method.memberOf === symbol.alias) {
					for (var k = 0, n = method.calls.length; k < n; k++) {
						var call = method.calls[k].desc.match(/([^\s]+)(?:\s+(.*))?/);
						var desc = (call[2] ? " " + call[2] : "");
						call = call[1];

						Link.currentSymbol = symbol;
						method.calls[k] = call + resolveLinks(desc);

						if (call.indexOf("#") === 0) {
							call = symbol.alias + call;
						}
						if (!callMap[call]) {
							callMap[call] = [];
						}

						// Items starting with #. or #- are valid on the page for the symbol to which they belong
						// but when translating for other pages it's the character after, not the #, that matters.
						desc = desc.replace(/\{@link #(\W)/g, "{@link " + symbol.alias + "$1").replace(/\{@link #/g, "{@link " + symbol.alias + "#");
						callMap[call].push(method.alias + resolveLinks(desc));
					}
				}
			}
		}
		if (JSDOC.opt.D.augments) {
			
			if (!augMap[symbol.alias]) {
				augMap[symbol.alias] = [];
			}
			symbol.augmentors = augMap[symbol.alias];
		   
			for (var j = 0, m = symbol.augments.length; j < m; j++) {
				var augmented = symbol.augments[j];
				
				if (!augMap[augmented]) {
					augMap[augmented] = [];
				}
				augMap[augmented].push(symbol);
			}
		}
	}
	delete Link.currentSymbol;
	if (classes[0].properties.length === 0 && classes[0].methods.length === 0 && classes[0].events.length === 0) {
		classes = classes.slice(1);
	}
	
	// create a class index, displayed in the left-hand column of every class page
	Link.base = "../";
 	publish.classesIndex = classesTemplate.process(classes); // kept in memory
	publish.header = headerTemplate.process(JSDOC.opt.D.header? IO.readFile(SYS.userDir+SYS.slash+JSDOC.opt.D.header) : "");
	
	// create each of the class pages
	for (var i = 0, l = classes.length; i < l; i++) {
		var symbol = classes[i];

		Link.currentSymbol= symbol;
		var output = "";
		output = classTemplate.process(symbol);
		
		IO.saveFile(publish.conf.outDir+"symbols/", ((JSDOC.opt.u)? Link.filemap[symbol.alias] : symbol.alias) + publish.conf.ext, output);
	}
	
	// regenerate the index with different relative links, used in the index pages
	Link.base = "";
	publish.classesIndex = classesTemplate.process(classes);
	
	// create the class index page
	try {
		var classesindexTemplate = new JSDOC.JsPlate(publish.conf.templatesDir+"index.tmpl");
	}
	catch(e) { print(e.message); quit(); }
	
	var classesIndex = classesindexTemplate.process(classes);
	IO.saveFile(publish.conf.outDir, "index"+publish.conf.ext, classesIndex);
	classesindexTemplate = classesIndex = classes = null;
	
	// create the file index page
	if (!JSDOC.opt.s) {
	try {
		var fileindexTemplate = new JSDOC.JsPlate(publish.conf.templatesDir+"allfiles.tmpl");
	}
	catch(e) { print(e.message); quit(); }
	}
	
	var documentedFiles = symbols.filter(isaFile); // files that have file-level docs
	var allFiles = []; // not all files have file-level docs, but we need to list every one
	
	for (var i = 0; i < files.length; i++) {
		allFiles.push(new JSDOC.Symbol(files[i], [], "FILE", new JSDOC.DocComment("/** */")));
	}
	
	for (var i = 0; i < documentedFiles.length; i++) {
		var offset = files.indexOf(documentedFiles[i].alias);
		allFiles[offset] = documentedFiles[i];
	}
		
	allFiles = allFiles.sort(makeSortby("name"));

	// output the file index page
	if (!JSDOC.opt.s) {
	var filesIndex = fileindexTemplate.process(allFiles);
	IO.saveFile(publish.conf.outDir, "files"+publish.conf.ext, filesIndex);
	fileindexTemplate = filesIndex = files = null;
	}
	files = filesIndexTemplate = null;
}


/** Just the first sentence (up to a full stop). Should not break on dotted variable names. */
function summarize(desc) {
	if (typeof desc != "undefined")
		return desc.match(/([\w\W]+?\.)[^a-z0-9_$]/i)? RegExp.$1 : desc;
}

/** Make a symbol sorter by some attribute. */
function makeSortby(attribute) {
	return function(a, b) {
		if (a[attribute] != undefined && b[attribute] != undefined) {
			a = a[attribute].toLowerCase();
			b = b[attribute].toLowerCase();
			if (a < b) return -1;
			if (a > b) return 1;
			return 0;
		}
	}
}

/** Pull in the contents of an external file at the given path. */
function include(path) {
	var path = publish.conf.templatesDir+path;
	return IO.readFile(path);
}

/** Turn a raw source file into a code-hilited page in the docs. */
function makeSrcFile(path, srcDir, name) {
	if (JSDOC.opt.s) return;
	
	if (!name) {
		name = path.replace(/\.\.?[\\\/]/g, "").replace(/[\\\/]/g, "_");
		name = name.replace(/\:/g, "_");
	}
	
	var src = {path: path, name:name, charset: IO.encoding, hilited: ""};
	
	if (defined(JSDOC.PluginManager)) {
		JSDOC.PluginManager.run("onPublishSrc", src);
	}

	if (src.hilited) {
		IO.saveFile(srcDir, name+publish.conf.ext, src.hilited);
	}
}

/** Build output for displaying function parameters. */
function makeSignature(params) {
	if (!params) return "()";
	var signature = "("
	+
	params.filter(
		function($) {
			return $.name.indexOf(".") == -1; // don't show config params in signature
		}
	).map(
		function($) {
			return $.name;
		}
	).join(", ")
	+
	")";
	return signature;
}

/** Find symbol {@link ...} strings in text and turn into html links */
function resolveLinks(str, from) {
	str = str.replace(/\{@link ([^} ]+) ?\}/gi,
		function(match, symbolName) {
			return new Link().toSymbol(symbolName);
		}
	);
	
	return str;
}
